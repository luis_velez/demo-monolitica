package com.monolitica.demo;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.monolitica.demo.util.Calculadora;

@SpringBootTest(classes = DemoMonoliticaApplication.class)
public class DemoMonoliticaApplicationTest {
	private final static Logger log = Logger.getLogger(DemoMonoliticaApplicationTest.class);
	
	@Autowired
	private Calculadora calculadora;

	@Test
	public void classLoader() {	
		log.info("Test: Suma de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		calculadora = new Calculadora();
		assertThat(calculadora.suma(2.0, 5.0), is((7.0)));
		log.info("Result: " + calculadora.getResult());	
	}

}
