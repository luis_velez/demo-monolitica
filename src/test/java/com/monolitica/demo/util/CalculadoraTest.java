package com.monolitica.demo.util;

import static org.assertj.core.api.Assertions.not;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.monolitica.demo.util.Calculadora;
	
public class CalculadoraTest {

	private final static Logger log = Logger.getLogger(CalculadoraTest.class);

	@Test
	public void suma() {
		log.info("Test: Suma de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		Calculadora c = new Calculadora();
		assertThat(c.suma(2.0, 5.0), is((7.0)));
		log.info("Result: " + c.getResult());
	}

	@Test
	public void multipicacion() {
		log.info("Test: multipicacion de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		Calculadora c = new Calculadora();
		assertThat(c.multipicacion(2.0, 5.0), is((10.0)));
		log.info("Result: " + c.getResult());
	}

	@Test
	public void resta() {
		log.info("Test: resta de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		Calculadora c = new Calculadora();
		assertThat(c.resta(15.0, 5.0), is((10.0)));
		log.info("Result: " + c.getResult());
	}

	@Test(expected = GenericException.class)
	public void division() throws GenericException {
		log.info("Test: division de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		Calculadora c = new Calculadora();
		assertThat(c.division(20.0, 5.0), is((4.0)));
		log.info("Result: " + c.getResult());
	}

	@Test(expected = GenericException.class)
	public void division2() throws GenericException {
		log.info("Test: Suma de doubles.");
		log.info("Num1: " + 2.0);
		log.info("Num2: " + 5.0);
		Calculadora c = new Calculadora();
			assertThat(c.division(20.0, 0.0), is(not(4.0)));
	}
}
