package com.monolitica.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMonoliticaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoMonoliticaApplication.class, args);
	}

}
